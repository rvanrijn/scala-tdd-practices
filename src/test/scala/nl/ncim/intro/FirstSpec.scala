package nl.ncim.intro

import collection.mutable.Stack
import org.scalatest._

/**
  * http://www.scalatest.org/user_guide/writing_your_first_test
  *
  * Class FlatSpec is a good first step for teams wishing to move from xUnit to BDD,
  * because its structure is flat like xUnit, so simple and familiar, but the test names
  * must be written in a specification style: “X should Y,” “A must B,” etc.
  */
class StackSpec extends FlatSpec {

  "A Stack" should "pop values in last-in-first-out order" in {
    val stack = new Stack[Int]
    stack.push(1)
    stack.push(2)
    assert(stack.pop() === 2)
    assert(stack.pop() === 1)
  }

  it should "throw NoSuchElementException if an empty stack is popped" in {
    val emptyStack = new Stack[String]
    intercept[NoSuchElementException] {
      emptyStack.pop()
    }
  }
}
