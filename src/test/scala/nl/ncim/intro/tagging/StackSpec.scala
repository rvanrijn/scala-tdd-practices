package nl.ncim.intro.tagging

import org.scalatest.FlatSpec
import scala.collection.mutable.Stack

/**
  * http://www.scalatest.org/user_guide/tagging_your_tests
  *
  *
  */
class StackSpec extends FlatSpec {

  "A Stack" should "pop values in last-in-first-out order" in {
    val stack = new Stack[Int]
    stack.push(1)
    stack.push(2)
    assert(stack.pop() === 2)
    assert(stack.pop() === 1)
  }

  //It will run only the first test and report that the second test was ignored:
  ignore should "throw NoSuchElementException if an empty stack is popped" in {
    val emptyStack = new Stack[String]
    intercept[NoSuchElementException] {
      emptyStack.pop()
    }
  }
}
