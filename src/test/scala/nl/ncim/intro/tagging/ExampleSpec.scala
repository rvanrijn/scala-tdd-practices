package nl.ncim.intro.tagging

import org.scalatest.FlatSpec

/**
  * http://www.scalatest.org/user_guide/using_the_runner#filtering
  *
  */
class ExampleSpec extends FlatSpec {

  import org.scalatest.Tag

  object SlowTest extends Tag("com.mycompany.tags.SlowTest")
  object DbTest extends Tag("com.mycompany.tags.DbTest")

  "The Scala language" must "add correctly" taggedAs(SlowTest) in {
    val sum = 1 + 1
    assert(sum === 2)
  }

  it must "subtract correctly" taggedAs(SlowTest, DbTest) in {
    val diff = 4 - 1
    assert(diff === 3)
  }
}
