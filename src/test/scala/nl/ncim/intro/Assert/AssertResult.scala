package nl.ncim.intro.assert

/**
  * http://www.scalatest.org/user_guide/using_assertions
  *
  * Use assertResult to differentiate expected from actual values;
  *
  */
object AssertResult {

  def main(args: Array[String]): Unit = {

    import org.scalatest.Assertions.assertResult
    val a = 5
    val b = 2
    assertResult(2) {
      a - b
    }

  }

}
