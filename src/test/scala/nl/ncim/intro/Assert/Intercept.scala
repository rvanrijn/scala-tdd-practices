package nl.ncim.intro.assert

/**
  * http://www.scalatest.org/user_guide/using_assertions
  *
  * Use intercept to ensure a bit of code throws an expected exception.
  *
  */
object Intercept {

  def main(args: Array[String]): Unit = {

    import org.scalatest.Assertions.{fail, intercept}

    val s1 = "hi"
    try {
      s1.charAt(-1)
      fail()
    }
    catch {
      case _: IndexOutOfBoundsException => println("Im here now") // Expected, so continue
    }


    val s2 = "hi"
    intercept[IndexOutOfBoundsException] {
      println("Im here now")
      s2.charAt(-1)
    }
  }
}
