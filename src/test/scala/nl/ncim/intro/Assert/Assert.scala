package nl.ncim.intro.assert

/**
  * http://www.scalatest.org/user_guide/using_assertions
  *
  * Use assert for general assertions;
  */
object Assert {

  def main(args: Array[String]): Unit = {

    //import org.scalatest.Assertions._
    val left = 2
    val right = 1

    //If false, Scala's assert will complete abruptly with an AssertionError.
    assert(left == right)
  }
}
