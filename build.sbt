name := "scala-tdd-practices"

scalaVersion := "2.11.7"

organization := "NCIM Groep"

libraryDependencies ++= Seq(
  "org.scalatest" % "scalatest_2.11" % "2.0" % "test",
  "org.scalamock" % "scalamock-scalatest-support_2.11" % "3.2.2",
  "org.scalacheck" %% "scalacheck" % "1.12.5" % "test"
)

// append options passed to the Scala compiler
scalacOptions ++= Seq("-deprecation", "-unchecked")

fork in run := true